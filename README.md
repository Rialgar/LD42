# Title goes here

### Made for LD 42

### No Idea what it will be, yet

[![Primary Screenshot](https://placehold.it/800x600)](https://rialgar.gitlab.io/LD42/index.xhtml)
[![Secondary Screenshot](https://placehold.it/800x600)](https://rialgar.gitlab.io/LD42/index.xhtml)

### [Play here](https://rialgar.gitlab.io/LD42/index.xhtml)
### [Rate here](https://ldjam.com/events/ludum-dare/42/)
TODO: change the above to the actual game page once published!

Tested in Chrome, should work in FF. Probably will work in Opera, Safari and Edge. Don't use IE.!